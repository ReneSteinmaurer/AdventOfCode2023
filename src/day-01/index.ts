import * as fs from 'fs';

const words = fs.readFileSync('src/day-01/input.txt', 'utf-8');
const inputList = words.split('\r\n');
const numberWords: { [key: string]: string } = {
    'one': '1', 'two': '2', 'three': '3', 'four': '4', 'five': '5',
    'six': '6', 'seven': '7', 'eight': '8', 'nine': '9'
};

const testList = [
    'two1nine',
    'eightwothree',
    'abcone2threexyz',
    'xtwone3four',
    '4nineeightseven2',
    'zoneight234',
    '7pqrstsixteen'
]

function findFirstNumber(str: string): string | null {
    for (let i = 0; i < str.length; i++) {
        for (let word in numberWords) {
            if (str.substring(i).startsWith(word)) {
                return numberWords[word];
            }
        }
        if (str[i].match(/\d/)) {
            return str[i];
        }
    }
    return null;
}

function findLastNumber(str: string): string | null {
    for (let i = str.length - 1; i >= 0; i--) {
        for (let word in numberWords) {
            if (str.substring(0, i + 1).endsWith(word)) {
                return numberWords[word];
            }
        }
        if (str[i].match(/\d/)) {
            return str[i];
        }
    }
    return null;
}

function getNumberSumFromString(str: string): number {
    let firstNumber = findFirstNumber(str);
    let lastNumber = findLastNumber(str);

    if (firstNumber && lastNumber) {
        return parseInt(firstNumber + lastNumber);
    } else {
        return 0;
    }
}

let sumOfAllNumbers = inputList.reduce((sum, line) => sum + getNumberSumFromString(line), 0);

console.log(sumOfAllNumbers);