import fs from "fs";

interface Card {
    id: number,
    numbers: number[];
    winningNumbers: number[];
}

function parseCardData(line: string): Card {
    const parts = line.split('|');
    const idPart = parts[0].split(':')[0];
    const numbersPart = parts[0].split(':')[1];
    const winningNumbersPart = parts[1];

    const id = parseInt(idPart.replace('Card', '').trim());
    const numbers = numbersPart.trim().split(/\s+/).map(Number);
    const winningNumbers = winningNumbersPart.trim().split(/\s+/).map(Number);

    return { id, numbers, winningNumbers };
}

function readCardsFromFile(filePath: string): Card[] {
    const fileContent = fs.readFileSync(filePath, 'utf-8');
    const lines = fileContent.split('\n');
    return lines.map(parseCardData);
}

function getSumOfWinningNumbersFromCard(card: Card) {
    const winningNumbersSet = new Set(card.winningNumbers);
    let sum = 0;

    for (const number of card.numbers) {
        if (winningNumbersSet.has(number)) {
            if (sum == 0) sum += 1;
            else sum *= 2;
        }
    }

    return sum;
}



let testCards: Card[] =
    [
        {
            id: 1,
            numbers: [41, 48, 83, 86, 17],
            winningNumbers: [83, 86, 6, 31, 17, 9, 48, 53],
        },
        {
            id: 2,
            numbers: [13, 32, 20, 16, 61],
            winningNumbers: [61, 30, 68, 82, 17, 32, 24, 19],
        },
        {
            id: 3,
            numbers: [1, 21, 53, 59, 44],
            winningNumbers: [69, 82, 63, 72, 16, 21, 14, 1],
        },
        {
            id: 4,
            numbers: [41, 92, 73, 84, 69],
            winningNumbers: [59, 84, 76, 51, 58, 5, 54, 83],
        },
        {
            id: 5,
            numbers: [87, 83, 26, 28, 32],
            winningNumbers: [88, 30, 70, 12, 93, 22, 82, 36],
        },
        {
            id: 6,
            numbers: [31, 18, 13, 56, 72],
            winningNumbers: [74, 77, 10, 23, 35, 67, 36, 11]
        }
    ]

let sumOfAll = 0;
const cards = readCardsFromFile('src/day-04/input.txt');

for (const card of cards) {
    sumOfAll += getSumOfWinningNumbersFromCard(card);
}

console.log(sumOfAll);